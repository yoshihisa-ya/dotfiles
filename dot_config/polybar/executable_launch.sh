#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

echo "Polybar launched..."
if [ $(hostname) == "desktop" ]; then
    MONITOR=DP-1-8 polybar --reload main &
    MONITOR=DP-1-1-8 polybar --reload secondary &
    MONITOR=HDMI-1 polybar --reload 4k &
elif type "xrandr"; then
    for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
        MONITOR=$m polybar --reload main &
    done
else
    polybar --reload main &
fi
